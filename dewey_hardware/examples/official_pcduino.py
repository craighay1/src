import os.path

class InvalidChannelException(ValueError):
    """The channel sent was invalid."""

    def __init__(self, pin):
        super(InvalidChannelException, self).__init__("pin %s not found" % pin)


class PinMap(object):
    def __init__(self, path, prefix, count):
        self.pins = ['%s%s' % (prefix, i) for i in xrange(count)]
        self.path = path

    def get_path(self, pin, path=None):
        """Get path of pin fd.

        pin can either be the pin basename (i.e. 'adc2') or pin number (i.e. 2)
        if prefix is supplied, override the default path prefix.

        """
        if not path:
            path = self.path

        if isinstance(pin, int):
            try:
                pin = self.pins[pin]
            except IndexError:
                raise InvalidChannelException(pin)

        if not pin in self.pins:
            raise InvalidChannelException(pin)
        return os.path.join(path, pin)


class GPIO(object):
    __all__ = ['HIGH', 'LOW', 'INPUT', 'OUTPUT','digital_write', 'digital_read',
               "pin_mode"]

    HIGH = 1
    LOW = 0
    INPUT = 0
    OUTPUT = 1

    gpio_pins = PinMap(
        '/sys/devices/virtual/misc/gpio/pin',
        'gpio',
        20
    )

    gpio_mode_pins = PinMap(
        '/sys/devices/virtual/misc/gpio/mode/',
        'gpio',
        20
    )

    def digital_write(self, channel, value):
        """Write to a GPIO channel"""
        path = self.gpio_pins.get_path(channel)
        with open(path, 'w') as f:
            f.write('1' if value == HIGH else '0')

    def digital_read(self, channel):
        """Read from a GPIO channel"""
        path = self.gpio_pins.get_path(channel)
        with open(path, 'r') as f:
            return f.read(1) == '1'

    def pin_mode(self, channel, mode):
        """ Set Mode of a GPIO channel """
        path = self.gpio_mode_pins.get_path(channel)
        with open(path, 'w+') as f:
            f.write('0' if mode == INPUT else '1')


class PWM(object):
    pwm_enable = PinMap(
        '/sys/devices/virtual/misc/pwmtimer/pin',
        'pwm',
        20
    )

    pwm_freq = PinMap(
        '/sys/devices/virtual/misc/pwmtimer/freq/',
        'pwm',
        20
    )

    pwm_freq_range = PinMap(
        '/sys/devices/virtual/misc/pwmtimer/freq_range/',
        'pwm',
        20
    )

    pwm_level = PinMap(
        '/sys/devices/virtual/misc/pwmtimer/level/',
        'pwm',
        20
    )

    pwm_max_level = PinMap(
        '/sys/devices/virtual/misc/pwmtimer/maxlevel/',
        'pwm',
        20
    )

    MAX_PWM_LEVEL = 255

    def analog_write(pin, value):
        """Write to one of the pwm pins.

        value can be an number between 0 and 255.

        """
        path = pins.get_path(pin)

        with open(os.path.join(path, 'max_brightness')) as f:
            max_value = int(f.read())

        if value < 0 or value > MAX_PWM_LEVEL:
            raise ValueError("value must be between 0 and %s" % MAX_PWM_LEVEL)

        map_level = (max_value * value) / MAX_PWM_LEVEL
        with open(os.path.join(path, 'brightness'), 'w+') as f:
            f.write("%d\n" % map_level)


class ADC(object):
    pins = PinMap('/proc', 'adc', 6)

    def analog_read(self, channel):
        """Return the integer value of an adc pin.

        adc0 and adc1 have 6 bit resolution.
        adc2 through adc5 have 12 bit resolution.

        """
        with open(self.pins.get_path(channel), 'r') as f:
            return int(f.read(32).split(':')[1].strip())