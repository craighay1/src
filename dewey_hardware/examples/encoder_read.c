#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#include <linux/input.h>

int main(int argc, char **argv)
{
	int fd;
	struct input_event ie;
	struct timeval last;
	double scaling = 45.0; //How many mouse units per millimeter
	double update_interval = 0.2; //How often to update in seconds
	double xdist = 0.0, ydist = 0.0, tdiff, xvel = 0.0, yvel = 0.0;

	if(argc != 2) {
		fprintf(stderr, "Usage: %s /dev/input/eventX\n", argv[0]);
		return -1;
	}

	if((fd = open(argv[1], O_RDONLY)) == -1) {
		perror(argv[1]);
		exit(EXIT_FAILURE);
	}

	//Main loop - if not multi-threaded just use a read with O_NONBLOCK set and check if read is NULL or use an I/O signal handler
	gettimeofday(&last, NULL);

	while(read(fd, &ie, sizeof(struct input_event))) {
		//See linux/input.h for decoding event codes
		if(ie.type == EV_REL) {
			switch(ie.code)
			{
				case REL_X:
					xdist += (double)ie.value / scaling;
					break;
				case REL_Y:
					ydist += (double)ie.value / scaling;
					break;
			}
		}

		//Update speed periodically
		tdiff = (double)(ie.time.tv_sec - last.tv_sec) + ((double)(ie.time.tv_usec - last.tv_usec) / 1000000.0);
		if(tdiff > update_interval)
		{
			//Get time delta and update last event time
			last = ie.time;
			xvel = (xdist/tdiff)/2;
			yvel = (ydist/tdiff)/2;
		}

		printf("time %ld.%06ld\ttype %d\tcode %d\tval %d\ttdiff %.03f\tXdist %.03f\tYdist %.03f\tXvel %.03f\tYvel %.03f\n",
		       ie.time.tv_sec, ie.time.tv_usec, ie.type, ie.code, ie.value, tdiff, xdist, ydist, xvel, yvel);
	}

	if(close(argv[1]) == -1)
		perror(argv[1]);

	return 0;
}