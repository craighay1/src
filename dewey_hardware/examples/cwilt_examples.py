import os
from fcntl import ioctl
from array import array
from os import getpid
import signal


class Pin:
    """
    # MypcDuinoIO.py
    """
    GPIO_MODE_PATH = os.path.normpath('/sys/devices/virtual/misc/gpio/mode/')
    GPIO_PIN_PATH = os.path.normpath('/sys/devices/virtual/misc/gpio/pin/')
    GPIO_FILENAME = "gpio"
    thepin = ''

    def __init__(self, mypin, mode):
        self.gpio = mypin
        self.mode = mode
        self.pinMode = (os.path.join(self.GPIO_MODE_PATH, 'gpio'+str(self.gpio)))
        self.pinData = (os.path.join(self.GPIO_PIN_PATH, 'gpio'+str(self.gpio)))
        fd = open(self.pinMode, 'r+')
        fd.write(str(self.mode))
        fd.close()
        self.fd = open(self.pinData, 'r+', 0)

    def high(self):
        self.fd.write("1")
        self.thepin = 1

    def low(self):
        self.fd.write("0")
        self.thepin = 0

    def toggle(self):
        if self.thepin == 0:
            self.high()
        else:
            self.low()

    def state(self):
        self.fd.seek(0)
        return int(self.fd.read(1))

    def setmode(self, x):
        self.mode = x
        fd = open(self.pinMode, 'r+')
        fd.write(str(self.mode))
        fd.close()

    def __del__(self):
        self.fd.close()


class ADC:
    """
    ADC
    """
    ADC_PATH = os.path.normpath('/proc/')
    # raw = 0

    def __init__(self, pin):
        self.adc = pin
        if self.adc == 0 or self.adc == 1:
            self.adcMult = 2/62
        else:
            self.adcMult = 3.3/4095
        self.adcData = (os.path.join(self.ADC_PATH, 'adc'+str(self.adc)))
        self.fd = open(self.adcData, 'r')

    def raw(self):
        self.fd.seek(0)
        value = self.fd.read(9)
        value = int(value[5:len(value)])
        return value

    def volts(self):
        self.fd.seek(0)
        value = self.fd.read(9)
        value = int(value[5:len(value)])
        if value == 0:
            return 0
        else:
            return '%.4f' % (value*self.adcMult)

    def __del__(self):
        self.fd.close()


class PWM:
    """
    PWM.py
    """
    pwm_enable_path = '/sys/devices/virtual/misc/pwmtimer/enable/pwm'
    pwm_freq_path = '/sys/devices/virtual/misc/pwmtimer/freq/pwm'
    pwm_duty_path = '/sys/devices/virtual/misc/pwmtimer/level/pwm'
    pwm_maxlevel_path = '/sys/devices/virtual/misc/pwmtimer/max_level/pwm'
    pwm_freqrange_path = '/sys/devices/virtual/misc/pwmtimer/freq_range/pwm'
    pwm_gpiomode_path = '/sys/devices/virtual/misc/gpio/mode/gpio'

    def __init__(self, thepin):
        self.Mypin = thepin
        self.isactive = 0
        fd = open(self.pwm_enable_path+str(self.Mypin), 'r+')
        fd.write('0')
        fd.close()
        if self.Mypin == 3 or self.Mypin == 9 or self.Mypin == 10 or self.Mypin == 11:
            self.max_freq = 8000
            self.min_freq = 2
            self.mymaxlevel = 63
            fd = open(self.pwm_gpiomode_path+str(self.Mypin), 'r+')
            fd.write('1')
            fd.close()
        elif self.Mypin == 5 or self.Mypin == 6:
            self.max_freq = 64000
            self.min_freq = 2
            self.mymaxlevel = 255
            fd = open(self.pwm_gpiomode_path+str(self.Mypin), 'r+')
            fd.write('2')
            fd.close()
        else:
            self.max_freq = None
            self.min_freq = None

    def maxfreq(self):
        return self.max_freq

    def minfreq(self):
        return self.min_freq

    def currentduty(self):
        fd = open(self.pwm_duty_path+str(self.Mypin), 'r')
        temp = int(fd.readline())
        fd.close()
        return temp

    def isactive(self):
        fd = open(self.pwm_enable_path+str(self.Mypin), 'r')
        temp = int(fd.read(1))
        fd.close()
        return temp

    def freq(self, speed):
        myspeed = speed
        if (myspeed >= self.min_freq) and (myspeed <= self.max_freq):
            fd = open(self.pwm_freq_path+str(self.Mypin), 'r+')
            fd.seek(0)
            fd.write(str(myspeed))
            fd.close()
            fd = open(self.pwm_maxlevel_path+str(self.Mypin), 'r')
            self.mymaxlevel = int(fd.readline())
            fd.close()

    def maxduty(self):
        return self.mymaxlevel

    def duty(self, dt):
        myduty = dt
        if (myduty >= 0) and (myduty <= self.mymaxlevel):
            fd = open(self.pwm_duty_path+str(self.Mypin), 'r+')
            fd.write(str(myduty))
            fd.close()

    def enable(self):
        fd = open(self.pwm_enable_path+str(self.Mypin), 'r+')
        fd.write('1')
        fd.close()
        self.isactive = 1

    def disable(self):
        fd = open(self.pwm_enable_path+str(self.Mypin), 'r+')
        fd.write('0')
        fd.close()
        self.isactive = 0


class SwIrq:
    """
    swirq.py
    """
    # from fcntl import ioctl
    # from array import array
    # from os import getpid
    # import signal

    def __init__(self):
        self.SWIRQ_START = 0X201
        self.SWIRQ_STOP = 0X202
        self.SWIRQ_SETPID = 0X203
        self.SWIRQ_ENABLE = 0X204
        self.SWIRQ_DISABLE = 0X205
        self.SWIRQ_LOW = 0X3
        self.SWIRQ_FALLING = 0X1
        self.SWIRQ_RISING = 0X0
        self.SWIRQ_CHANGE = 0X4
        self.SWIRQ_PATH = '/dev/swirq'

    def enable(self):
        """
        :return:
        """
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        irqnum = array('h', [0])
        ret = ioctl(fd, self.SWIRQ_ENABLE, irqnum, 1)
        print "enable 0 = ", ret
        if ret < 0:
            print "Can't set interrupt 0"
        # TODO: Is this needed? irqnum = []
        irqnum = array('h', [1])
        ret = ioctl(fd, self.SWIRQ_ENABLE, irqnum, 1)
        print "enable 1 = ", ret
        if ret < 0:
            print "Can't set interrupt 1"
        fd.close()
        return None

    def attach(self, intnum, mode, userfunc):
        """
        :param intnum:
        :param mode:
        :param userfunc:
        :return:
        """
        # hwmode initialises to HIGH
        hwmode = 0x2

        # TODO: What is this if statement doing?
        if mode == 'LOW':
            hwmode = 0x3
        elif mode == 'FALLING':
            hwmode = 0x1
        elif mode == 'RISING':
            hwmode = 0x0
        elif mode == 'CHANGE':
            hwmode = 0x4
        elif mode == 'HIGH':
            hwmode = 0x2

        # TODO: What is this if statement doing?
        if intnum == 0:
            signal.signal(signal.SIGUSR1, userfunc)
        elif intnum == 1:
            signal.signal(signal.SIGUSR2, userfunc)

        mypid = getpid()
        print "sigusr1 = ", signal.SIGUSR1
        print "sigusr2 = ", signal.SIGUSR2
        print intnum
        print hwmode
        print mypid
        irqconfig = array('h', [intnum, hwmode, mypid])
        irqnum = array('h', [intnum])
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        ret = ioctl(fd, self.SWIRQ_STOP, irqnum, 1)
        print "stop = ", ret
        if ret < 0:
            print "can't stop"
        ret = ioctl(fd, self.SWIRQ_SETPID, irqconfig, 1)
        print "setpid = ", ret
        if ret < 0:
            print "can't set"
        ret = ioctl(fd, self.SWIRQ_START, irqnum, 1)
        print "start = ", ret
        if ret < 0:
            print "can't start"
        fd.close()
        return None

    def detach(self, intnum):
        """
        :param intnum:
        :return:
        """
        irqnum = array('h', [intnum])
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        ret = ioctl(file, self.SWIRQ_STOP, irqnum, 1)
        if ret < 0:
            print "can't stop"
        fd.close()
        return None

    def disable(self):
        """
        :return:
        """
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        irqnum = array('h', [0])
        ret = ioctl(file, self.SWIRQ_DISABLE, irqnum, 1)
        if ret < 0:
            print "cant disable 0"
        # TODO: Is this needed? irqnum = []
        irqnum = array('h', [1])
        ret = ioctl(file, self.SWIRQ_DISABLE, irqnum, 1)
        if ret < 0:
            print "cant disable 1"
        fd.close()
        return None