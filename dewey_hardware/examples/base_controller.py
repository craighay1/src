#!/usr/bin/env python
# base_controller.py
# From ROS by Example book

import rospy
from geometry_msgs.msg import Twist, Point, Quaternion
import tf # monitors transformation between /odom and /base_link frames
from math import radians, copysign, sqrt, pow, pi

width_robot = 0.1  # wheel base
vl = 0  # linear velocity
vr = 0  # radial velocity
right_enc = 0  # right encoder count
left_enc = 0  # left encoder count
left_enc_old = 0  # previous left encoder count
right_enc_old = 0  # previous right encoder count
distance_left = 0
distance_right = 0
ticks_per_meter = 100  # number of encoder ticks to travel 1 metre
x = 0
y = 0
th = 0

'''
def cmd_velCallback(geometry_msgs::Twist &twist_aux):
    return 0
'''

# Give the node a name
rospy.init_node('base_controller', anonymous=False)

# Set rospy to execute shutdown function when exiting
rospy.on_shutdown(self.shutdown)

# Publisher to control the robot's speed
cmd_vel = rospy.Publisher('cmd_vel', Twist)

# How fast will we update the robot's movement
rate = 10

# Set equivalent ROS rate variable
r = rospy.Rate(rate)

# Set the forward linear velocity to 0.2m/s
vl = 0.2

# Set the travel distance in metres
goal_distance = 1.0

# Set the rotation speed in radians per second
vr = 1.0

# Set the angular tolerance in degrees converted to radians
# Very easy to overshoot rotation
# Slight angular error can send the robot far away from next location
# Value needs to be found empirically. Start with 2.5 degrees.
angular_tolerance = radians(2.5)

# Set the rotation angle to Pi radians (180 degrees)
goal_angle = pi

# Initialize the tf listener. This monitors frame transforms.
tf_listener = tf.TransformListener()

# Give the tf time to fill its buffer
rospy.sleep(2)

# Set the odom frame
odom_frame = '/odom'

# Find out if the robot uses /base_link or /base_footprint
try:
    tf_listener.waitForTransform(odom_frame, '/base_footprint', rospy.Time(), rospy.Duration(1.0))
    base_frame = '/base_footprint'
except (tf.Exception, tf.ConnectivityException, tfLookupException):
    try:
        tf_listener.waitForTransform(odom_frame, '/base_link', rospy.Time(), rospy.Duration(1.0))
        base_frame = '/base_link'
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        rospy.loginfo("Cannot find transform between /odom and /base_link or /base_footprint")
        rospy. signal_shutdown("tf Exception")

# Initialize the position variable as a Point type
position = Point()

# Loop once for each leg of the trip
for i in range(2):
    # Initialize movement command
    move_cmd = Twist()
    
    # Set the movement command to forward motion
    move_cmd.linear.x = linear_speed
    
    # Get the starting position values
    (position, rotation) = get_odom()
    
    x_start = position.x
    y_start = position.y
    
    # Keep track of the distance travelled
    distance = 0
    
    # Enter the loop to move along a side
    while distance < goal_distance and not rospy.is_shutdown():
        # Publish the Twist message and sleep one cycle
        cmd_vel.publish(move_cmd)
        
        r.sleep()
        
        # Get the current position
        (position, rotation) = get_odom()
        
        # Compute the Euclidean distance from the start
        distance = sqrt(pow((position.x - x_start), 2) + pow((position.y - y_start), 2))
    
    # Stop the robot before the rotation
    move_cmd = Twist()
    cmd_vel.publish(move_cmd)
    rospy.sleep(1)
    
    # Set the movement command to a rotation
    move_cmd.angular.z = angular_speed

# Track the last angle measured
last_angle = rotation

# Track how far we have turned
turn_angle = 0

while abs(turn_angle + angular_tolerance) < abs(goal_angle) and not rospy.is_shutdown():
    # Publish the Twist message and sleep 1 cycle
    self.cmd_vel.publish(move_cmd)
    r.sleep()
    
    # Get the current rotation
    (position, rotation) = get_odom()
    
    # Compute the amount of rotation since the last loop
    delta_angle = normalize_angle(rotation - last_angle)
    
    # Add to the running total
    turn_angle += delta_angle
    last_angle = rotation
    
    # Stop the robot before the next leg
    move_cmd = Twist()
    cmd_vel.publish(move_cmd)
    rospy.sleep(1)
    
    # Stop the robot for good
    cmd_vel.publish(Twist())

def get_odom(self):
    # Get the current transform between the odom and base frames
    try:
        (trans, rot) = self.tf_listener.lookupTransform(self.odom_frame, self.base_frame, rospy.Time(0))
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        rospy.loginfo("TFF Exception")
        return
    
    return (Point(*trans). quat_to_angle(Quaternion(*rot)))

def shutdown(self):
    # Always stop the robot when shutting down the node
    rospy.loginfo("Stopping the robot..."
    self.cmd_vel.publish(Twist())
    rospy.sleep(1)
