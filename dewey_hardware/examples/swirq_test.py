import swirq_example
import time

mycount=0
def myreturn(signum,stacknum):
    global mycount
    print'Recieved=',signum
    mycount+=1

swirq_example.enable()
swirq_example.attach(0,'RISING',myreturn)
var=1
while var<100:
    print 'pass=',var
    time.sleep(5)
    var+=1
    if mycount>5:
        swirq_example.detach(0)
        quit()
