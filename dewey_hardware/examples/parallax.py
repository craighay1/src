package stamp.peripheral.sensor;
import stamp.core.*;
/**
 * This class provides an interface to the Parallax PING))) ultrasonic
 * range finder module.
 * <p>
 * <i>Usage:</i><br>
 * <code>
 * Ping range = new Ping(CPU.pin0); // trigger and echo on P0
 * </code>
 * <p>
 * Detailed documentation for the PING))) Sensor can be found at: <br>
 * http://www.parallax.com/detail.asp?product_id=28015
 * <p>
 *
 * @version 1.0 03 FEB 2005
 */
public final class Ping {
 private int ioPin;
 /**
 * Creates PING))) range finder object
 *
 * @param ioPin PING))) trigger and echo return pin
 */
 public Ping (int ioPin) {
 this.ioPin = ioPin;
 }
 /**
 * Returns raw distance value from the PING))) sensor.
 *
 * @return Raw distance value from PING)))
 */
 public int getRaw() {
 int echoRaw = 0;
 CPU.writePin(ioPin, false); // setup for high-going pulse
 CPU.pulseOut(1, ioPin); // send trigger pulse
 echoRaw = CPU.pulseIn(2171, ioPin, true); // measure echo return
 // return echo pulse if in range; zero if out-of-range
 return (echoRaw < 2131) ? echoRaw : 0;
 }
 Parallax, Inc. • PING)))TM Ultrasonic Distance Sensor (#28015) • v1.3 6/13/2006 Page 12 of 13
 /*
 * The PING))) returns a pulse width of 73.746 uS per inch. Since the
 * Javelin pulseIn() round-trip echo time is in 8.68 uS units, this is the
 * same as a one-way trip in 4.34 uS units. Dividing 73.746 by 4.34 we
 * get a time-per-inch conversion factor of 16.9922 (x 0.058851).
 *
 * Values to derive conversion factors are selected to prevent roll-over
 * past the 15-bit positive values of Javelin Stamp integers.
 */
 /**
 * @return PING))) distance value in inches
 */
 public int getIn() {
 return (getRaw() * 3 / 51); // raw * 0.058824
 }
 /**
 * @return PING))) distance value in tenths of inches
 */
 public int getIn10() {
 return (getRaw() * 3 / 5); // raw / 1.6667
 }
 /*
 * The PING))) returns a pulse width of 29.033 uS per centimeter. As the
 * Javelin pulseIn() round-trip echo time is in 8.68 uS units, this is the
 * same as a one-way trip in 4.34 uS units. Dividing 29.033 by 4.34 we
 * get a time-per-centimeter conversion factor of 6.6896.
 *
 * Values to derive conversion factors are selected to prevent roll-over
 * past the 15-bit positive values of Javelin Stamp integers.
 */
 /**
 * @return PING))) distance value in centimeters
 */
 public int getCm() {
 return (getRaw() * 3 / 20); // raw / 6.6667
 }
 /**
 * @return PING))) distance value in millimeters
 */
 public int getMm() {
 return (getRaw() * 3 / 2); // raw / 0.6667
 }



 import stamp.core.*;
import stamp.peripheral.sensor.Ping;
public class testPing {
 public static final char HOME = 0x01;
 public static void main() {
 Ping range = new Ping(CPU.pin0);
 StringBuffer msg = new StringBuffer();
 int distance;
 while (true) {
 // measure distance to target in inches
 distance = range.getIn();
 // create and display measurement message
 msg.clear();
 msg.append(HOME);
 msg.append(distance);
 msg.append(" \" \n");
 System.out.print(msg.toString());
 // wait 0.5 seconds between readings
 CPU.delay(5000);
 }
 }
}



'''
Parallax's Ping))) sensor is an ultrasonic sensor that is used for distance measurements.

Ultrasonic sensors work by sending out a sound wave and waiting until that wave bounces back to the sensor. This means that the sensor's accuracy can actually change with the speed of sound. However, this is usually not an issue.

The advantage of the Ping sensor over similar ultrasonic sensors is that it only has three pins: +5V, GND, Trigger. This means that one less pin is used on the Arduino, which may be crucial to a project.

Coding the Ping
Coding the Ping is very simple since there is an Arduino example code included in the IDE under: File >> Examples >> 06.Sensors >> Ping.

This gives the basic code to get it Ping operating. To integrate this into code, it may not be necessary to read the sensor everytime, so putting a timer on it may be advantageous:
'''
previousMillis = millis();
PingPin = 7;

def setup():
    Serial.begin(9600)

def loop():
    interval = 1000
    duration = 0.0

    if(millis() - previousMillis > interval):
        # Ping is triggered
        pinMode(pingPin, OUTPUT)
        digitalWrite(pingPin, LOW)
        delayMicroseconds(2)
        digitalWrite(pingPin, HIGH)
        delayMicroseconds(5)
        digitalWrite(pingPin, LOW)
        pinMode(pingPin, INPUT)
        duration = pulseIn(pingPin, HIGH)
        previousMillis = millis()
'''
This is a simple way to trigger the sensor only when the interval of time has passed.
'''

'''
Python using RPi.GPIO library
'''
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)

#cleanup output
GPIO.output(11, 0)

time.sleep(0.000002)

#send signal
GPIO.output(11, 1)

time.sleep(0.000005)

GPIO.output(11, 0)

GPIO.setup(11, GPIO.IN)

while GPIO.input(11)==0:
   starttime=time.time()

while GPIO.input(11)==1:
   endtime=time.time()

duration=endtime-starttime
distance=duration*34000/2
print distance