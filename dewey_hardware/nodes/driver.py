#!/usr/bin/env python
# motor_driver.py
# Converted Arduino like code for the pcduino and motor driver

import os
# import subprocess as an alternative to os for writing to files/console
import rospy
from std_msgs.msg import Int16, Float32  # String

# imports for SwIrq class
from fcntl import ioctl
from array import array
from os import getpid
import signal



class GPIO(object):
    """
    GPIO class
    """

    def __init__(self, pin):
        """
        :param pin:
        :return:
        """
        # Create a few strings for file I/O equivalence
        self.high = "1"
        self.low = "0"
        self.set_input = "0"
        self.set_output = "1"
        self.enable = "1"
        self.disable = "0"
        self.input_pullup = "8"
        self.mypin = str(pin)

        # set system paths for GPIO
        self._gpio_mode_path = os.path.normpath('/sys/devices/virtual/misc/gpio/mode/gpio')
        self._gpio_data_path = os.path.normpath('/sys/devices/virtual/misc/gpio/pin/gpio')

        # create empty arrays to store the pointers for our files
        self.pinMode = []
        self.pinData = []
        self.ioPinMap = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)

        # First, populate the arrays with file objects that we can use later.
        for i in self.ioPinMap:
            self.pinMode.append(os.path.join(self._gpio_mode_path, str(i)))
            self.pinData.append(os.path.join(self._gpio_data_path, str(i)))
    
    def pinhigh(self):
        """
        :param pin:
        :return:
        """
        fd = open(self._gpio_data_path+self.mypin, 'r+')
        fd.write(self.high)
        fd.close()

    def pinlow(self):
        """
        :param pin:
        :return:
        """
        fd = open(self._gpio_data_path+self.mypin, 'r+')
        fd.write(self.low)
        fd.close()

    def enablepin(self):
        """
        :param pin:
        :return:
        """
        fd = open(self._gpio_mode_path+self.mypin, 'r+')
        fd.write(self.enable)
        fd.close()

    def disablepin(self, pin):
        """
        :param pin:
        :return:
        """
        fd = open(self._gpio_mode_path+self.mypin, 'r+')
        fd.write(self.disable)
        fd.close()
    
    def readpin(self, pin):
        """
        :param pin:
        :return:
        """
        fd = open(self.pinMode[pin], 'r+')
        fd.write(self.input_pullup)
        fd.close()
        temp = [""]
        fd = open(self.pinData[pin], 'r')
        temp[0] = fd.read()
        fd.close()
        return temp[0]


class PWM(object):
    """
    PWM class
    """

    # set system paths for pwm
    _pwm_gpio_mode_path = '/sys/devices/virtual/misc/gpio/mode/gpio'
    _pwm_enable_path = '/sys/devices/virtual/misc/pwmtimer/enable/pwm'
    _pwm_freq_path = '/sys/devices/virtual/misc/pwmtimer/freq/pwm'
    _pwm_duty_path = '/sys/devices/virtual/misc/pwmtimer/level/pwm'
    _pwm_max_level_path = '/sys/devices/virtual/misc/pwmtimer/max_level/pwm'
    _pwm_freq_range_path = '/sys/devices/virtual/misc/pwmtimer/freq_range/pwm'

    def __init__(self, pin):
        """
        :param pin:
        :return:
        """
        self.Mypin = pin
        fd = open(self._pwm_enable_path+str(self.Mypin), 'r+')
        fd.write('0')
        fd.close()
        self.active = 0
        if self.Mypin == 3 or self.Mypin == 9 or self.Mypin == 10 or self.Mypin == 11:
            self.max_freq = 8000
            self.min_freq = 2
            self.max_level = 63
            fd = open(self._pwm_gpio_mode_path+str(self.Mypin), 'r+')
            fd.write('1')
            fd.close()
        elif self.Mypin == 5 or self.Mypin == 6:
            self.max_freq = 64000
            self.min_freq = 2
            self.max_level = 255
            self.change_freq(2000, 50)
        else:
            self.max_freq = None
            self.min_freq = None

    def enable(self):
        """
        :return:
        """
        print "enabling pwm"
        string = "%s%d" % (self._pwm_enable_path, self.Mypin)
        fd = open(string, 'r+')
        fd.write('1')
        fd.close()
        self.active = 1
        print "pwm enabled"

    def disable(self):
        """
        :return:
        """
        print "disabling pwm"
        fd = open(self._pwm_enable_path+str(self.Mypin), 'r+')
        fd.write('0')
        fd.close()
        self.active = 0
        print "pwm disabled"

    def maxfreq(self):
        """
        :return:
        """
        return self.max_freq

    def minfreq(self):
        """
        :return:
        """
        return self.min_freq

    def currentduty(self):
        """
        :return:
        """
        fd = open(self._pwm_duty_path+str(self.Mypin), 'r')
        temp = int(fd.readline())
        fd.close()
        return temp
    
    def currentfreq(self):
        """
        :return:
        """
        fd = open(self._pwm_freq_path+str(self.Mypin), 'r')
        temp = int(fd.readline())
        fd.close()
        return temp

    def isactive(self):
        """
        :return:
        """
        fd = open(self._pwm_enable_path+str(self.Mypin), 'r')
        temp = int(fd.read(1))
        fd.close()
        return temp

    def freq(self, speed):
        """
        :param speed:
        :return:
        """
        myspeed = speed
        if (myspeed >= self.min_freq) and (myspeed <= self.max_freq):
            fd = open(self._pwm_freq_path+str(self.Mypin), 'r+')
            fd.seek(0)
            fd.write(str(myspeed))
            fd.close()
            fd = open(self._pwm_max_level_path+str(self.Mypin), 'r')
            self.mymaxlevel = int(fd.readline())
            fd.close()

    def maxduty(self):
        """
        :return:
        """
        return self.max_level

    def duty(self, dt):
        """
        :param dt:
        :return:
        """
        myduty = dt
        if (myduty >= 0) and (myduty <= self.max_level):
            fd = open(self._pwm_duty_path+str(self.Mypin), 'r+')
            fd.write(str(myduty))
            fd.close()

    def change_duty(self, level):
        """
        Level can be between 0-255 for pwm5&6, 0-32 for the rest.
        """

        # set duty cycle
        print "setting pwm duty cycle"
        fd = open(self._pwm_duty_path+str(self.Mypin), 'r+')
        fd.write('%d' % level)
        fd.close()
        print "pwm duty set to %d" % level

    def change_freq(self, freq, level):
        """
        Frequency can be between 2-63000 for pwm5&6, 2-8192 for the rest.
        Level can be between 0-255 for pwm5&6, 0-32 for the rest.
        :param pin:
        :param freq:
        :param level:
        :return:
        """

        fd = open(self._pwm_enable_path+str(self.Mypin), 'r+')
        fd.write('0')
        fd.close()
        if self.Mypin == 3 or self.Mypin == 9 or self.Mypin == 10 or self.Mypin == 11:
            self.max_freq = 8000
            self.min_freq = 2
            fd = open(self._pwm_gpio_mode_path+str(self.Mypin), 'r+')
            fd.write('1')
            fd.close()
        elif self.Mypin == 5 or self.Mypin == 6:
            self.max_freq = 64000
            self.min_freq = 2
            fd = open(self._pwm_gpio_mode_path+str(self.Mypin), 'r+')
            fd.write('2')
            fd.close()
        else:
            self.max_freq = None
            self.min_freq = None

        # disable pwms
        self.disable()

        # set frequency
        print "setting frequency"
        fd = open(self._pwm_freq_path+str(self.Mypin), 'r+')
        fd.write('%d' % freq)
        fd.close()

        # set duty cycle
        print "setting pwm duty cycle"
        self.change_duty(level)

        # enable pwms
        self.enable()

        print "ending routine"


class ADC(object):

    # set system paths for analog input
    _adc_path = os.path.normpath('/proc')

    def __init__(self, pin):
        """
        :param pin:
        :return:
        """
        self.adc = pin

        # Setting the voltage levels (pins 1 and 2 are 2V, pins 3, 4 and 5 are 3.3V
        if self.adc == 0 or self.adc == 1:
            self.adcMult = 2/62
        else:
            self.adcMult = 3.3/4095

        self.fd = open(self.adcData, 'r')
        # create empty arrays to store the pointers for our files
        self.adcFiles = []
        self.adcPinMap = (0, 1, 2, 3, 4, 5)

        self.adcData = (os.path.join(self._adc_path, 'adc'+str(self.adc)))
        for i in self.adcPinMap:
            self.adcFiles.append(os.path.join(self._adc_path, 'adc' + str(i)))
    
    def analog_read(self, pin):
        """
        :param pin:
        :return:
        """
        fd = open(pin, 'r')
        fd.seek(0)
        print "ADC Channel: " + str(self.adcFiles.index(pin)) + " Result: " + fd.read(16)
        fd.close()

    def raw(self):
        self.fd.seek(0)
        value = self.fd.read(9)
        value = int(value[5:len(value)])
        return value

    def volts(self):
        self.fd.seek(0)
        value = self.fd.read(9)
        value = int(value[5:len(value)])
        if value == 0:
            return 0
        else:
            return '%.4f' % (value*self.adcMult)

    def __del__(self):
        self.fd.close()


class MotorDriver(object):
    """
    MotorDriver class
    """

    def __init__(self):
        """
        :return:
        """
        # Create a few strings for file I/O equivalence
        self.fwd = 1
        self.rev = -1
        self.rcoder = 0
        self.lcoder = 0
        self.lprev = 0
        self.rprev = 0
        self.ldir = 0
        self.rdir = 0
        self.A_IA = 4
        self.A_IB = 5
        self.B_IA = 6
        self.B_IB = 7
        print 'setup complete'

    def rforward(self, speed):
        """
        :param speed:
        :return:
        """
        print "motor 1 forward"
        GPIO.enablepin(GPIO.pinMode[9])
        GPIO.enablepin(GPIO.pinMode[8])
        GPIO.enablepin(GPIO.pinMode[11])
        GPIO.pinhigh(GPIO.pinData[9])
        # pwm.change_freq(pwm.Mypin[5], 30000, 200)
        GPIO.pinlow(GPIO.pinData[8])
        GPIO.pinhigh(GPIO.pinData[11])
        self.rdir = self.fwd
        print speed
        print "motor 1 is forwarding"
        return self.rdir

    def rreverse(self, speed):
        """
        :param speed:
        :return:
        """
        print "motor 1 reverse"
        self.enablepin(self.pinMode[9])
        self.enablepin(self.pinMode[8])
        self.enablepin(self.pinMode[11])
        self.pinhigh(self.pinData[9])
        self.pinhigh(self.pinData[8])
        self.pinlow(self.pinData[11])
        self.rdir = self.rev
        print speed
        print "motor 1 is reversing"
        return self.rdir

    def rbrake(self, speed):
        """
        :param speed:
        :return:
        """
        print "braking motor 1"
        self.enablepin(self.pinMode[9])
        self.enablepin(self.pinMode[8])
        self.enablepin(self.pinMode[11])
        self.pinlow(self.pinData[9])
        self.pinlow(self.pinData[8])
        self.pinlow(self.pinData[11])
        self.rdir = 0
        print speed
        print "motor 1 is off"
        return self.rdir

    def rcoast(self, speed):
        """
        :param speed:
        :return:
        """
        print "coasting motor 1"
        self.enablepin(self.pinMode[9])
        self.enablepin(self.pinMode[8])
        self.enablepin(self.pinMode[11])
        self.pinlow(self.pinData[9])
        self.pinlow(self.pinData[8])
        self.pinlow(self.pinData[11])
        self.rdir = 0
        print speed
        print "motor 1 is coasting"
        return self.rdir

    def lforward(self, speed):
        """
        :param speed:
        :return:
        """
        print "motor 2 forward"
        self.enablepin(self.pinMode[10])
        self.enablepin(self.pinMode[12])
        self.enablepin(self.pinMode[13])
        self.pinhigh(self.pinData[10])
        self.pinhigh(self.pinData[12])
        self.pinlow(self.pinData[13])
        self.ldir = self.fwd
        print speed
        print "motor 2 is forwarding"
        return self.ldir

    def lreverse(self, speed):
        """
        :param speed:
        :return:
        """
        print "motor 2 reverse"
        self.enablepin(self.pinMode[10])
        self.enablepin(self.pinMode[12])
        self.enablepin(self.pinMode[13])
        self.pinhigh(self.pinData[10])
        self.pinlow(self.pinData[12])
        self.pinhigh(self.pinData[13])
        self.ldir = self.rev
        print speed
        print "motor 2 is reversing"
        return self.ldir

    def lbrake(self, speed):
        """
        :param speed:
        :return:
        """
        print "braking motor 2"
        self.enablepin(self.pinMode[10])
        self.enablepin(self.pinMode[12])
        self.enablepin(self.pinMode[13])
        self.pinlow(self.pinData[10])
        self.pinlow(self.pinData[12])
        self.pinlow(self.pinData[13])
        self.ldir = 0
        print speed
        print "motor 2 is braking"
        return self.ldir

    def lcoast(self, speed):
        """
        :param speed:
        :return:
        """
        print "coasting motor 2"
        self.enablepin(self.pinMode[10])
        self.enablepin(self.pinMode[12])
        self.enablepin(self.pinMode[13])
        self.pinlow(self.pinData[10])
        self.pinlow(self.pinData[12])
        self.pinlow(self.pinData[13])
        self.ldir = 0
        print speed
        print "motor 2 is coasting"
        return self.ldir

    def rmotor_callback(self, motor_msg):
        """
        :param motor_msg:
        :return:
        """
        """
        A callback to deal with the right motor message.
        @type motor_msg: Float32
        """
        rospy.loginfo("rmotor_callback")
        if motor_msg.data > 255 or motor_msg.data < -255:
            self.rbrake(motor_msg.data)
        elif motor_msg.data == 0:
            self.rcoast(motor_msg.data)
        elif motor_msg.data < 0:
            self.rreverse(abs(motor_msg.data))
        else:
            self.rforward(motor_msg.data)

    def lmotor_callback(self, motor_msg):
        """
        A callback to deal with the left motor message.
        :type motor_msg: float
        """
        rospy.loginfo('lmotor_callback')
        if motor_msg.data > 255 or motor_msg.data < -255:
            self.lbrake(motor_msg.data)
        elif motor_msg.data == 0:
            self.lcoast(motor_msg.data)
        elif motor_msg.data < 0:
            self.lreverse(abs(motor_msg.data))
        else:
            self.lforward(motor_msg.data)

    def do_lcoder(self):
        """
        :return:
        """
        self.lcoder += self.ldir

    def do_rcoder(self):
        """
        :return:
        """
        self.lcoder += self.rdir


class Encoder(object):

    def __init__(self):
        self.count = 0
        return

    def count(self):
        return self.count


class SwIrq:
    """
    swirq.py
    """

    def __init__(self):
        self.SWIRQ_START = 0X201
        self.SWIRQ_STOP = 0X202
        self.SWIRQ_SETPID = 0X203
        self.SWIRQ_ENABLE = 0X204
        self.SWIRQ_DISABLE = 0X205
        self.SWIRQ_LOW = 0X3
        self.SWIRQ_FALLING = 0X1
        self.SWIRQ_RISING = 0X0
        self.SWIRQ_CHANGE = 0X4
        self.SWIRQ_PATH = '/dev/swirq'

    def enable(self):
        """
        :return:
        """
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        irqnum = array('h', [0])
        ret = ioctl(fd, self.SWIRQ_ENABLE, irqnum, 1)
        print "enable 0 = ", ret
        if ret < 0:
            print "Can't set interrupt 0"
        # TODO: Is this needed? irqnum = []
        irqnum = array('h', [1])
        ret = ioctl(fd, self.SWIRQ_ENABLE, irqnum, 1)
        print "enable 1 = ", ret
        if ret < 0:
            print "Can't set interrupt 1"
        fd.close()
        return None

    def attach(self, intnum, mode, userfunc):
        """
        :param intnum:
        :param mode:
        :param userfunc:
        :return:
        """
        # hwmode initialises to HIGH
        hwmode = 0x2

        # TODO: What is this if statement doing?
        if mode == 'LOW':
            hwmode = 0x3
        elif mode == 'FALLING':
            hwmode = 0x1
        elif mode == 'RISING':
            hwmode = 0x0
        elif mode == 'CHANGE':
            hwmode = 0x4
        elif mode == 'HIGH':
            hwmode = 0x2

        # TODO: What is this if statement doing?
        if intnum == 0:
            signal.signal(signal.SIGUSR1, userfunc)
        elif intnum == 1:
            signal.signal(signal.SIGUSR2, userfunc)

        mypid = getpid()
        print "sigusr1 = ", signal.SIGUSR1
        print "sigusr2 = ", signal.SIGUSR2
        print "intnum = ", intnum
        print "hwmode = ", hwmode
        print "mypid = ", mypid
        irqconfig = array('h', [intnum, hwmode, mypid])
        irqnum = array('h', [intnum])
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        ret = ioctl(fd, self.SWIRQ_STOP, irqnum, 1)
        print "stop = ", ret
        if ret < 0:
            print "can't stop"
        ret = ioctl(fd, self.SWIRQ_SETPID, irqconfig, 1)
        print "setpid = ", ret
        if ret < 0:
            print "can't set"
        ret = ioctl(fd, self.SWIRQ_START, irqnum, 1)
        print "start = ", ret
        if ret < 0:
            print "can't start"
        fd.close()
        return None

    def detach(self, intnum):
        """
        :param intnum:
        :return:
        """
        irqnum = array('h', [intnum])
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        ret = ioctl(file, self.SWIRQ_STOP, irqnum, 1)
        if ret < 0:
            print "can't stop"
        fd.close()
        return None

    def disable(self):
        """
        :return:
        """
        fd = open(self.SWIRQ_PATH, 'r+')
        if fd < 0:
            print "file open fail"
        irqnum = array('h', [0])
        ret = ioctl(fd, self.SWIRQ_DISABLE, irqnum, 1)
        if ret < 0:
            print "cant disable 0"
        # TODO: Is this needed? irqnum = []
        irqnum = array('h', [1])
        ret = ioctl(fd, self.SWIRQ_DISABLE, irqnum, 1)
        if ret < 0:
            print "cant disable 1"
        fd.close()
        return None


def setup():
    # setting up PWM pins on 5 and 6
    lmotor_speed = PWM(5)
    rmotor_speed = PWM(6)
    lmotor_speed.enable()
    rmotor_speed.enable()

    # Setting up digital outputs on 4 and 7
    lmotor_direction = GPIO(4)
    rmotor_direction = GPIO(7)
    lmotor_direction.enablepin()
    rmotor_direction.enablepin()
    lmotor_direction.pinlow() # Initally set low
    rmotor_direction.pinlow()
    rospy.loginfo("setup complete")

    # setting encoder pins
    rwheel = GPIO(2)
    lwheel = GPIO(3)

    # setting up a motor driver
    driver = MotorDriver()

def main():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    setup()
    rprev = 0
    lprev = 0
    lcoder = 0
    rcoder = 0
    rdir = 0
    ldir = 0
    tick_no = 0
    ticks_since_beat = 0

    rospy.init_node('motor_driver', anonymous=False, log_level=rospy.INFO)  # disable_signals=FALSE
    rate = rospy.Rate(50)  # 10hz
    lmotor_sub = rospy.Subscriber("lmotor_cmd", Float32, MotorDriver.lmotor_callback)
    rmotor_sub = rospy.Subscriber("rmotor_cmd", Float32, MotorDriver.rmotor_callback)
    lwheel_pub = rospy.Publisher("lwheel", Int16, queue_size=10)
    rwheel_pub = rospy.Publisher("rwheel", Int16, queue_size=10)


'''
    # spin() simply keeps python from exiting until this node is stopped
    # rospy.spin()
    while not rospy.is_shutdown():
        lwheel_pub.publish(lcoder)
        rwheel_pub.publish(rcoder)
   
        if rprev != GPIO.readpin(driver.rwheel):
            rcoder += rdir
            rprev = GPIO.readpin(driver.rwheel)
        if lprev != GPIO.readpin(driver.lwheel):
            lcoder += ldir
            lprev = GPIO.readpin(driver.lwheel)

            rate.sleep()
        '''

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass

'''
    nh.spinOnce();
    ticks_since_beat++;

    if(ticks_since_beat > (1000 / LOOP_DLY) ) {
        tick_no++;
        sprintf(debug_str, "tick %d", tick_no);
        msg_debug.data = debug_str;
        debug_pub.publish( &msg_debug );
        ticks_since_beat = 0;
    }

    // try without interrupts

//    if (rprev != digitalRead(RWHEEL)) {
//        rcoder += rdir;
//        rprev = digitalRead(RWHEEL);
//    }
//    if (lprev != digitalRead(LWHEEL)){
//        lcoder += ldir;
//        lprev = digitalRead(LWHEEL);
//    }
    msg_lwheel.data = lcoder;
    msg_rwheel.data = rcoder;
    msg_range.data = analogRead( RANGE );
    msg_scope.data = analogRead( SCOPE );
    lwheel_pub.publish( &msg_lwheel );
    rwheel_pub.publish( &msg_rwheel );
    range_pub.publish( &msg_range );
    scope_pub.publish( &msg_scope );


    nh.spinOnce();

    delay(LOOP_DLY);
'''

'''
def dimLED():
    pwmMaxVal = []
    for file in pwmMaxLevel:
        fd = open(file, 'r')
        pwmMaxVal.append(int(fd.read(16)))
        fd.close()
    print pwmMaxVal

    for file in pwmLevel:
        j = pwmLevel.index(file)
        print j
        for i in range(0,pwmMaxVal[j]):
            cmd = "echo " + str(i) + " > " + file
            print cmd
            disablePWM()
            subprocess.check_output(cmd, shell=True) 
            enablePWM()
            time.sleep(.2)

def setup_test():
    disableallpins()
    enablepin(pinMode[8])
    pinlow(pinData[8])
    enablepin(pinMode[11])
    pinlow(pinData[11])
    enablepwm()
    change_freq(1000, 150)
    print "start motor"
    pinhigh(pinData[11])
    time.sleep(1)
    change_freq(1000, 170)
    print "increase speed"
    time.sleep(1)
    change_freq(1000, 190)
    print "increase speed"
    time.sleep(3)
    print "change direction and speed"
    pinlow(pinData[11])
    time.sleep(.5)
    pinhigh(pinData[8])
    change_freq(1000, 120)
    time.sleep(3)
    print "motor off"
    pinlow(pinData[8])

import PWM

Mypwm=PWM.pwm(5)
print Mypwm.maxfreq()
print Mypwm.minfreq()
Mypwm.freq(30000)
tmp=Mypwm.maxduty()
print tmp
Mypwm.duty(int(tmp/2))
print Mypwm.currentduty()
Mypwm.enable()'''
