#!/usr/bin/env python
# motor_driver.py
# Converted Arduino like code for the pcduino and motor driver

import os
import time
import rospy
from std_msgs.msg import Int16, Float32 # String
# import subprocess as an alternative to os for writing to files/console

# Create a few strings for file I/O equivalence
HIGH = "1"
LOW = "0"
INPUT = "0"
ENABLE = "1"
DISABLE = "0"
ENABLE_PWM = "2"
DIGI_SET_OUTPUT = "1"
INPUT_PULLUP = "8"

global FWD
FWD = 1
global REV
REV = -1
RWHEEL = 2
LWHEEL = 3
# lcoder = 0
# rcoder = 0
# lprev = 0
# rprev = 0
# ldir = 0
# rdir = 0

# set system paths for GPIO
GPIO_MODE_PATH = os.path.normpath('/sys/devices/virtual/misc/gpio/mode/')
GPIO_DATA_PATH = os.path.normpath('/sys/devices/virtual/misc/gpio/pin/')

# set system paths for PWM
PWM_PATH = os.path.normpath('/sys/devices/virtual/misc/pwmtimer/')
PWM_ENABLE = "enable"
PWM_FREQ = "freq"
PWM_MAX_FREQ = "freq_range"
PWM_LEVEL = "level"
PWM_MAX_LEVEL = "max_level"

# set system paths for analog input
ADC_PATH = os.path.normpath('/proc')

# create empty arrays to store the pointers for our files
pinMode = []
pinData = []
pwmMode = []
pwmFreq = []
pwmMaxFreq = []
pwmLevel = []
pwmMaxLevel = []
adcFiles = []
ioPinMap = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
pwmPinMap = (0, 0, 0, 3, 0, 5, 6, 0, 0, 9, 10, 11)
adcPinMap = (0, 1, 2, 3, 4, 5)

# Creating lists of file objects to use later
for i in ioPinMap:
    pinMode.append(os.path.join(GPIO_MODE_PATH, 'gpio' + str(i)))
    pinData.append(os.path.join(GPIO_DATA_PATH, 'gpio' + str(i)))

for i in pwmPinMap:
    if i != 0:
        pwmMode.append(os.path.join(PWM_PATH, PWM_ENABLE, 'pwm' + str(i)))
        pwmFreq.append(os.path.join(PWM_PATH, PWM_FREQ, 'pwm' + str(i)))
        pwmLevel.append(os.path.join(PWM_PATH, PWM_LEVEL, 'pwm' + str(i)))
        pwmMaxFreq.append(os.path.join(PWM_PATH, PWM_MAX_FREQ, 'pwm' + str(i)))
        pwmMaxLevel.append(os.path.join(PWM_PATH, PWM_MAX_LEVEL, 'pwm' + str(i)))

for i in adcPinMap:
    adcFiles.append(os.path.join(ADC_PATH, 'adc' + str(i)))

# setting motor driver pins
A_IA = pinData[4]
A_IB = pinData[5]
B_IA = pinData[6]
B_IB = pinData[7]

# print 'setup complete'


def readpin(pin):
    fd = open(pinMode[pin], 'r+')
    fd.write(INPUT_PULLUP)
    fd.close()
    temp = [""]
    fd = open(pinData[pin], 'r')
    temp[0] = fd.read()
    fd.close()
    # print temp[0]
    return temp[0]


def enablepin(pin):
    fd = open(pinMode[pin], 'r+')
    fd.write(ENABLE)
    fd.close()


def disablepin(pin):
    fd = open(pinMode[pin], 'r+')
    fd.write(DISABLE)
    fd.close()


def pinhigh(pin):
    fd = open(pinData[pin], 'r+')
    fd.write(HIGH)
    fd.close()


def pinlow(pin):
    fd = open(pinData[pin], 'r+')
    fd.write(LOW)
    fd.close()


def enablepwm():
    fd = open(pinMode[5], 'r+')
    fd.write(ENABLE_PWM)
    fd.close()


def rforward(speed):
    # print "motor 1 forward"
    enablepin(pinMode[4])
    enablepin(pinMode[5])
    pinhigh(pinData[4])
    pinlow(pinData[5])
    global rdir
    rdir = FWD
    # print speed
    # print "motor 1 is forwarding"
    return rdir


def rreverse(speed):
    # print "motor 1 reverse"
    enablepin(pinMode[4])
    enablepin(pinMode[5])
    pinhigh(pinData[4])
    pinhigh(pinData[5])
    global rdir
    rdir = REV
    # print speed
    # print "motor 1 is reversing"
    return rdir


def rbrake(speed):
    # print "braking motor 1"
    enablepin(pinMode[4])
    enablepin(pinMode[5])
    pinlow(pinData[4])
    pinlow(pinData[5])
    global rdir
    rdir = 0
    # print speed
    # print "motor 1 is off"
    return rdir


def rcoast(speed):
    # print "coasting motor 1"
    enablepin(pinMode[4])
    enablepin(pinMode[5])
    pinlow(pinData[4])
    pinlow(pinData[5])
    global rdir
    rdir = 0
    # print speed
    # print "motor 1 is coasting"
    return rdir


def lforward(speed):
    # print "motor 2 forward"
    enablepin(pinMode[6])
    enablepin(pinMode[7])
    pinhigh(pinData[6])
    pinhigh(pinData[7])
    global ldir
    ldir = FWD
    # print speed
    # print "motor 2 is forwarding"
    return ldir


def lreverse(speed):
    # print "motor 2 reverse"
    enablepin(pinMode[6])
    enablepin(pinMode[7])
    pinhigh(pinData[6])
    pinlow(pinData[7])
    global ldir
    ldir = REV
    # print speed
    # print "motor 2 is reversing"
    return ldir


def lbrake(speed):
    # print "braking motor 2"
    enablepin(pinMode[6])
    enablepin(pinMode[7])
    pinlow(pinData[6])
    pinlow(pinData[7])
    global ldir
    ldir = 0
    # print speed
    # print "motor 2 is braking"
    return ldir


def lcoast(speed):
    # print "coasting motor 2"
    enablepin(pinMode[6])
    enablepin(pinMode[7])
    pinlow(pinData[6])
    pinlow(pinData[7])
    global ldir
    ldir = 0
    # print speed
    # print "motor 2 is coasting"
    return ldir


def change_freq(freq, level):
    """
    Frequency can be between 2-63000 for pwm5&6, 2-8192 for the rest.
    Level can be between 0-255 for pwm5&6, 0-32 for the rest.
    """
    # disable pwms
    # print "disabling pwm"
    for pin in pwmMode:
        fd = open(pin, 'r+')
        fd.write(DISABLE)
        fd.close()
        # time.sleep(2)

    # set frequency
    # print "setting frequency"
    for pin in pwmFreq:
        fd = open(pin, 'r+')
        fd.write('%d' % freq)
        fd.close()
        # time.sleep(2)

    # set duty cycle
    # print "setting pwm duty cycle"
    for pin in pwmLevel:
        fd = open(pin, 'r+')
        fd.write('%d' % level)
        fd.close()
        # time.sleep(2)

    # enable pwms
    # print "enabling pwm"
    for pin in pwmMode:
        fd = open(pin, 'r+')
        fd.write(ENABLE)
        fd.close()
        # time.sleep(2)

    # print "ending routine"


def change_duty(level):
    """
    Level can be between 0-255 for pwm5&6, 0-32 for the rest. 
    """

    # set duty cycle
    # print "setting pwm duty cycle"
    for pin in pwmLevel:
        fd = open(pin, 'r+')
        fd.write('%d' % level)
        fd.close()
        # time.sleep(2)
    # print "pwm duty set to %d" % level


def analog_read(pin):
    fd = open(pin, 'r')
    fd.seek(0)
    # print "ADC Channel: " + str(adcFiles.index(pin)) + " Result: " + fd.read(16)
    fd.close()


def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)


def rmotor_callback(motor_msg):
    """
    A callback to deal with the right motor message.
    @type motor_msg: Float32
    """
    # rospy.loginfo("rmotor_callback")
    doREncoder()
    if motor_msg.data > 255 or motor_msg.data < -255:
        rbrake(motor_msg.data)
    elif motor_msg.data == 0:
        rcoast(motor_msg.data)
    elif motor_msg.data < 0:
        rreverse(abs(motor_msg.data))
    else:
        rforward(motor_msg.data)


def lmotor_callback(motor_msg):
    """
    A callback to deal with the left motor message.
    :type motor_msg: float
    """
    # rospy.loginfo('lmotor_callback')
    doLEncoder()
    if motor_msg.data > 255 or motor_msg.data < -255:
        lbrake(motor_msg.data)
    elif motor_msg.data == 0:
        lcoast(motor_msg.data)
    elif motor_msg.data < 0:
        lreverse(abs(motor_msg.data))
    else:
        lforward(motor_msg.data)


def doLEncoder():
   # self.lcoder += self.ldir
    global lprev, ldir, lcoder
    if lprev != readpin(LWHEEL):
        lcoder += ldir
        lprev = readpin(LWHEEL)


def doREncoder():
    # self.lcoder += self.rdir
    global rprev, rdir, rcoder
    if rprev != readpin(RWHEEL):
        rcoder += rdir
        rprev = readpin(RWHEEL)

'''
def main():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    global rprev
    rprev = 0
    global lprev
    lprev = 0
    global lcoder
    lcoder = 0
    global rcoder
    rcoder = 0
    global rdir
    rdir = 0
    global ldir
    ldir = 0
    tick_no = 0
    ticks_since_beat = 0

    rospy.init_node('motor_driver', anonymous=False, log_level=rospy.INFO)  # disable_signals=FALSE
    rate = rospy.Rate(1)  # 10hz
    lmotor_sub = rospy.Subscriber("lmotor_cmd", Float32, lmotor_callback)
    rmotor_sub = rospy.Subscriber("rmotor_cmd", Float32, rmotor_callback)
    lwheel_pub = rospy.Publisher("lwheel", Int16, queue_size=100)
    rwheel_pub = rospy.Publisher("rwheel", Int16, queue_size=100)



    # spin() simply keeps python from exiting until this node is stopped
    # rospy.spin()
    while not rospy.is_shutdown():
        lwheel_pub.publish(lcoder)
        rwheel_pub.publish(rcoder)
        # if rprev != readpin(RWHEEL):
        # rcoder += rdir
        # rprev = readpin(RWHEEL)
        # if lprev != readpin(LWHEEL):
        # lcoder += ldir
        # lprev = readpin(LWHEEL)

        rate.sleep()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass


    nh.spinOnce();
    ticks_since_beat++;

    if(ticks_since_beat > (1000 / LOOP_DLY) ) {
        tick_no++;
        # sprintf(debug_str, "tick %d", tick_no);
        msg_debug.data = debug_str;
        debug_pub.publish( &msg_debug );
        ticks_since_beat = 0;
    }

    // try without interrupts
//    if (rprev != digitalRead(RWHEEL)) {
//      rcoder += rdir;
//      rprev = digitalRead(RWHEEL);
//    }
//    if (lprev != digitalRead(LWHEEL)){
//      lcoder += ldir;
//      lprev = digitalRead(LWHEEL);
//    }
    msg_lwheel.data = lcoder;
    msg_rwheel.data = rcoder;
    msg_range.data = analogRead( RANGE );
    msg_scope.data = analogRead( SCOPE );
    lwheel_pub.publish( &msg_lwheel );
    rwheel_pub.publish( &msg_rwheel );
    range_pub.publish( &msg_range );
    scope_pub.publish( &msg_scope );


    nh.spinOnce();

    delay(LOOP_DLY);
'''

'''
def dimLED():
    pwmMaxVal = []
    for file in pwmMaxLevel:
        fd = open(file, 'r')
        pwmMaxVal.append(int(fd.read(16)))
        fd.close()
    # print pwmMaxVal

    for file in pwmLevel:
        j = pwmLevel.index(file)
        # print j
        for i in range(0,pwmMaxVal[j]):
            cmd = "echo " + str(i) + " > " + file
            # print cmd
            disablePWM()
            subprocess.check_output(cmd, shell=True) 
            enablePWM()
            time.sleep(.2)

def enableAllPWM():
    for pwm in pwmMode:
        enablePin(pwm)


def disableAllPWM():
    for pwm in pwmMode:
        disablePin(pwm)


def flash():
    x = 0
    while x < 10:
        enableAllPWM()
        time.sleep(1)
        disableAllPWM()
        time.sleep(1)
        enableAllPWM()
        time.sleep(1)
        disableAllPWM()
        x += 1


def setup_test():
    disableallpins()
    enablepin(pinMode[8])
    pinlow(pinData[8])
    enablepin(pinMode[11])
    pinlow(pinData[11])
    enablepwm()
    change_freq(1000, 150)
    # print "start motor"
    pinhigh(pinData[11])
    time.sleep(1)
    change_freq(1000, 170)
    # print "increase speed"
    time.sleep(1)
    change_freq(1000, 190)
    # print "increase speed"
    time.sleep(3)
    # print "change direction and speed"
    pinlow(pinData[11])
    time.sleep(.5)
    pinhigh(pinData[8])
    change_freq(1000, 120)
    time.sleep(3)
    # print "motor off"
    pinlow(pinData[8])
'''

# # print 'disabled PWM'
# disableAllPWM()
# time.sleep(5)
# # print 'enabled PWM'
# enableAllPWM()
# time.sleep(5)
# # print 'dimming LEDs'
# dimLED()
# # print 'exiting'
# Implement some checks in changeFreq...
# check level doesn't exceed 255
# if level < 1 || level > 32:
# # print 'levelout of range'
# break
# check frequency is in range
