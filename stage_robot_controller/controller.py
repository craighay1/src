import rospy
from math import *
from matrix import *
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent


def callback(event, cmd):
    # Callback function that executes obstacle avoidance on bumper button hits
    # BumperEvent.state can be .PRESED=1 or .RELEASED=0
    # BumperEvent.bumper can be .LEFT=0, .CENTER=1 or .RIGHT=2
    if event.state == event.PRESSED:
        rospy.loginfo("Bumper Hit!")
        if event.bumper == event.LEFT:
            rospy.loginfo("Left Bumper Hit!")
            cmd[1].linear.x *= -1.0
            cmd[1].angular.z *= -1.0
            print cmd[1].linear.x
            print cmd[1].angular.z
        elif event.bumper == event.CENTER:
            rospy.loginfo("Centre Bumper Hit!")
            cmd[1].linear.x *= -1.0
            cmd[1].angular.z *= -1.0
            print cmd[1].linear.x
            print cmd[1].angular.z
        elif event.bumper == event.RIGHT:
            rospy.loginfo("Right Bumper Hit!")
            cmd[1].linear.x *= -1.0
            cmd[1].angular.z *= -1.0
            print cmd[1].linear.x
            print cmd[1].angular.z
    return


def update(mean1, var1, mean2, var2):
    new_mean = float(var2 * mean1 + var1 * mean2) / (var1 + var2)
    new_var = 1./(1./var1 + 1./var2)
    return [new_mean, new_var]


def predict(mean1, var1, mean2, var2):
    new_mean = mean1 + mean2
    new_var = var1 + var2
    return [new_mean, new_var]


def kalmanfilter():
    # measurements = [5., 6., 7., 9., 10.]
    # motion = [1., 1., 2., 1., 1.]
    # measurement_sig = 4.
    # motion_sig = 2.
    # mu = 0.
    # sig = 10000.

    measurements = [1, 2, 3]

    x = matrix([[0.], [0.]]) # initial state (location and velocity)
    P = matrix([[1000., 0.], [0., 1000.]]) # initial uncertainty
    u = matrix([[0.], [0.]]) # external motion
    F = matrix([[1., 1.], [0, 1.]]) # next state function
    H = matrix([[1., 0.]]) # measurement function
    R = matrix([[1.]]) # measurement uncertainty
    I = matrix([[1., 0.], [0., 1.]]) # identity matrix

    for n in range(len(measurements)):
        [mu, sig] = update(mu, sig, measurements[n], measurement_sig)
        print 'update: ', [mu, sig]
        [mu, sig] = predict(mu, sig, motion[n], motion_sig)
        print 'predict: ', [mu, sig]

    Z = matrix([[measurements[n]]])
    y = Z - (H * x)
    S = H * P * H.transpose() + R
    K = P * H.transpose() * S.inverse()
    x = x + (K * y)

    P = (I - (K * H)) * P

    # prediction
    x = (F * x) + u
    P = F * P * F.transpose()



def main():
    # initialises my node which subscribes to bump sensor and publishes velocities
    rospy.init_node('turtlebot_controller')

    # Creates a Twist message called cmd
    # (a Twist message is just a data type used for linear and angular velocities)
    cmd = Twist()
    # creates a publisher that sends Twist messages to the robot
    cmd_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

    # creates a subscriber that waits for BumperEvent messages and then calls the callback
    rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, callback, [BumperEvent, cmd])

    # sets the rate of the control loop in Hertz
    rate = rospy.Rate(10)
    cmd.linear.x = 1.0 # set linear x velocity to 1m/s
    cmd.angular.z = 1.0 # set angular z velocity to 1rad/s

    # Main control loop that runs until shutdown command received
    while not rospy.is_shutdown():
        # MAIN CONTROL LOOP
        cmd_pub.publish(cmd)
        rate.sleep()

if __name__ == "__main__":
    main()