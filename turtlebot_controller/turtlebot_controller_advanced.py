import rospy
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent


<<<<<<< HEAD
def avoidance(self):
=======
def avoidance(cmd):
>>>>>>> 0e1f4fb5c05b127989538c8fa91aaddfefe65919
    """
    :param cmd: float
    :return:
    """
    # I want to write my avoidance routines in functions and make the callback call those functions.
    cmd.linear.x = -1.0
    cmd.angular.z = 1.0
    rospy.loginfo("Change linear direction. Now: " + str(cmd.linear.x))
    rospy.sleep(2)
    cmd.linear.x = 1.0
    cmd.angular.z = 1.0
    rospy.loginfo("Change linear direction. Now: " + str(cmd.linear.x))


<<<<<<< HEAD
def callback(self, msg):
=======
def callback(event, cmd):
>>>>>>> 0e1f4fb5c05b127989538c8fa91aaddfefe65919
    """
    Callback function that executes obstacle avoidance on bumper button hits
    :param event:
    :param avoidance:
    :param cmd:
    :return:
    """
    # BumperEvent.state can be .PRESED=1 or .RELEASED=0
    # BumperEvent.bumper can be .LEFT=0, .CENTER=1 or .RIGHT=2
    if event.state == event.PRESSED:
        rospy.loginfo("Bumper Hit!")
        if event.bumper == event.LEFT:
            rospy.loginfo("Left Bumper Hit!")
            avoidance(cmd[1])
        elif event.bumper == event.CENTER:
            rospy.loginfo("Centre Bumper Hit!")
            avoidance(cmd[1])
        elif event.bumper == event.RIGHT:
            rospy.loginfo("Right Bumper Hit!")
            avoidance(cmd[1])
    return

def main():
    # initialises my node which publishes and subscribes from
    rospy.init_node('turtlebot_controller')

    # creates a Twist message called cmd
    # (a Twist message is just a data type used for linear and angular velocities)
    cmd = Twist()

    # creates a publisher that sends Twist messages to the robot
    cmd_pub = rospy.Publisher('mobile_base/commands/velocity', Twist, queue_size=1)

    # creates a subscriber that waits for BumperEvent messages and then calls the callback
<<<<<<< HEAD
    rospy.Subscriber('/mobile_base/events/bumper', data_class=BumperEvent, callback=callback, callback_args=[msg])
=======
    rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, callback, [BumperEvent, cmd])
>>>>>>> 0e1f4fb5c05b127989538c8fa91aaddfefe65919

    # sets the rate of the control loop in Hertz
    rate = rospy.Rate(10)
    cmd.linear.x = 1.0 # set linear velocity to 1m/s
    cmd.angular.z = 1.0 # set angular velocity to 1rad/s

    # main control loop which runs until shutdown command received
    while not rospy.is_shutdown():
        cmd_pub.publish(cmd)
        rate.sleep()

if __name__ == "__main__":
    main()
