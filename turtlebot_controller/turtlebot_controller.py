import rospy
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import BumperEvent


def callback(self, event, cmd):
    # Callback function that executes obstacle avoidance on bumper button hits
    # BumperEvent.state can be .PRESED=1 or .RELEASED=0
    # BumperEvent.bumper can be .LEFT=0, .CENTER=1 or .RIGHT=2
    if event.state == event.PRESSED:
        rospy.loginfo("Bumper Hit!")
        if event.bumper == event.LEFT:
            rospy.loginfo("Left Bumper Hit!")
            cmd[1].linear.x *= -1.0
            cmd[1].angular.z *= -1.0
            print cmd[1].linear.x
            print cmd[1].angular.z
        elif event.bumper == event.CENTER:
            rospy.loginfo("Centre Bumper Hit!")
            cmd[1].linear.x *= -1.0
            cmd[1].angular.z *= -1.0
            print cmd[1].linear.x
            print cmd[1].angular.z
        elif event.bumper == event.RIGHT:
            rospy.loginfo("Right Bumper Hit!")
            cmd[1].linear.x *= -1.0
            cmd[1].angular.z *= -1.0
            print cmd[1].linear.x
            print cmd[1].angular.z
    return


def main(self):
    # initialises my node which subscribes to bump sensor and publishes velocities
    rospy.init_node('turtlebot_controller')

    # Creates a Twist message called cmd
    # (a Twist message is just a data type used for linear and angular velocities)
    cmd = Twist()
    # creates a publisher that sends Twist messages to the robot
    cmd_pub = rospy.Publisher('mobile_base/commands/velocity', Twist, queue_size=10)

    # creates a subscriber that waits for BumperEvent messages and then calls the callback
<<<<<<< HEAD
    rospy.Subscriber('/mobile_base/events/bumper', data_class=BumperEvent, callback=callback, callback_args=[event, cmd])
=======
    rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, callback, [BumperEvent, cmd])
>>>>>>> 0e1f4fb5c05b127989538c8fa91aaddfefe65919

    # sets the rate of the control loop in Hertz
    rate = rospy.Rate(10)
    cmd.linear.x = 1.0 # set linear x velocity to 1m/s
    cmd.angular.z = 1.0 # set angular z velocity to 1rad/s

    # Main control loop that runs until shutdown command received
    while not rospy.is_shutdown():
        # MAIN CONTROL LOOP
        cmd_pub.publish(cmd)
        rate.sleep()

if __name__ == "__main__":
    main()
